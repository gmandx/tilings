use rand::{thread_rng, Rng};

use super::super::constants::{MENU_BG_COLOR, TEXT_COLOR};
use super::*;

#[derive(Debug, PartialEq)]
struct Point {
    x: usize,
    y: usize,
}

#[derive(Debug)]
struct Cell {
    order: usize,
}

#[derive(Debug)]
pub struct GameView {
    width: usize,
    height: usize,
    grid: Vec<Cell>,
    empty_cell_index: usize,
    won: bool,
}

impl State for GameView {
    fn handle_event(&mut self, event: &Event) -> Option<CurrentState> {
        if let Event::Input(Input::Button(button_args)) = event {
            if button_args.state == ButtonState::Press {
                match button_args.button {
                    Button::Keyboard(Key::Up) => {
                        self.swap_up();
                    }
                    Button::Keyboard(Key::Down) => {
                        self.swap_down();
                    }
                    Button::Keyboard(Key::Left) => {
                        self.swap_left();
                    }
                    Button::Keyboard(Key::Right) => {
                        self.swap_right();
                    }

                    Button::Keyboard(Key::Escape) => {
                        return Some(CurrentState::Menu(MenuView::new(self.width, self.height)));
                    }
                    _ => {}
                }
            }
        }

        None
    }

    fn draw(&self, context: &Context, graphics: &mut G2d, glyphs: &mut Glyphs) {
        clear(*MENU_BG_COLOR, graphics);

        if self.won {
            text::Text::new_color(*TEXT_COLOR, 32)
                .draw(
                    &format!("You won!"),
                    glyphs,
                    &context.draw_state,
                    context.transform.trans(20.0, 50.0),
                    graphics,
                ).unwrap();

        } else {

            // let (viewport_width, viewport_height) = context
            //     .viewport
            //     .map_or((640, 480), |v| (v.rect[2], v.rect[3]));

            // let cell_side_length = min(viewport_width, viewport_height) /  ;

            for y in 0..self.height {
                for x in 0..self.width {
                    text::Text::new_color(*TEXT_COLOR, 32)
                        .draw(
                            &format!("{}", self.grid[y * self.width + x].order),
                            glyphs,
                            &context.draw_state,
                            context
                                .transform
                                .trans(20.0 + (50.0 * (x as f64)), 50.0 + (50.0 * (y as f64))),
                            graphics,
                        ).unwrap();
                }
            }
        }
    }
}

impl GameView {
    pub fn new(width: usize, height: usize) -> Self {
        let mut grid: Vec<Cell> = (0..(width * height)).map(|order| Cell { order }).collect();

        thread_rng().shuffle(&mut grid);

        let mut empty_cell_index = 0;
        for (i, c) in grid.iter().enumerate() {
            if c.order == 0 {
                empty_cell_index = i;
            }
        }

        Self {
            width,
            height,
            grid,
            empty_cell_index,
            won: false,
        }
    }

    fn coord_to_index(&self, point: Point) -> usize {
        point.y * self.width + point.x
    }

    fn index_to_coord(&self, index: usize) -> Point {
        Point {
            x: index % self.height,
            y: index / self.height,
        }
    }

    fn swap_up(&mut self) {
        let Point { x, y } = self.index_to_coord(self.empty_cell_index);
        if y <= 0 {
            return;
        }

        let swapped_cell_index = self.coord_to_index(Point { x, y: y - 1 });

        self.grid.swap(self.empty_cell_index, swapped_cell_index);

        self.empty_cell_index = swapped_cell_index;
        self.check_win();
    }

    fn swap_down(&mut self) {
        let Point { x, y } = self.index_to_coord(self.empty_cell_index);
        if y >= self.height - 1 {
            return;
        }

        let swapped_cell_index = self.coord_to_index(Point { x, y: y + 1 });

        self.grid.swap(self.empty_cell_index, swapped_cell_index);

        self.empty_cell_index = swapped_cell_index;
        self.check_win();
    }

    fn swap_left(&mut self) {
        let Point { x, y } = self.index_to_coord(self.empty_cell_index);
        if x <= 0 {
            return;
        }

        let swapped_cell_index = self.coord_to_index(Point { x: x - 1, y });

        self.grid.swap(self.empty_cell_index, swapped_cell_index);

        self.empty_cell_index = swapped_cell_index;
        self.check_win();
    }

    fn swap_right(&mut self) {
        let Point { x, y } = self.index_to_coord(self.empty_cell_index);
        if x >= self.width - 1 {
            return;
        }

        let swapped_cell_index = self.coord_to_index(Point { x: x + 1, y });

        self.grid.swap(self.empty_cell_index, swapped_cell_index);

        self.empty_cell_index = swapped_cell_index;
        self.check_win();
    }

    fn check_win(&mut self) {
        let mut last_order = self.grid[0].order;

        for c in self.grid.iter().skip(1) {
            if c.order > last_order {
                self.won = false;
                return;
            }
            last_order = c.order;
        }

        self.won = true;
    }
}
