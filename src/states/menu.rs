use super::super::constants::{MENU_BG_COLOR, TEXT_COLOR};
use super::*;

#[derive(Debug)]
pub struct MenuView {
    width: usize,
    height: usize,
}

impl State for MenuView {
    fn handle_event(&mut self, event: &Event) -> Option<CurrentState> {
        if let Event::Input(Input::Button(button_args)) = event {
            if button_args.state == ButtonState::Press {
                match button_args.button {
                    Button::Keyboard(Key::Up) => self.height += 1,
                    Button::Keyboard(Key::Down) => if self.height > 3 {
                        self.height -= 1;
                    },
                    Button::Keyboard(Key::Right) => self.width += 1,
                    Button::Keyboard(Key::Left) => if self.width > 3 {
                        self.width -= 1;
                    },
                    Button::Keyboard(Key::Return) => {
                        return Some(CurrentState::Game(GameView::new(self.width, self.height)));
                    }
                    Button::Keyboard(Key::Escape) => {
                        return Some(CurrentState::Exited);
                    }
                    _ => {}
                }
            }
        }

        None
    }

    fn draw(&self, context: &Context, graphics: &mut G2d, glyphs: &mut Glyphs) {
        clear(*MENU_BG_COLOR, graphics);
        if let Err(error) = text::Text::new_color(*TEXT_COLOR, 32).draw(
            &format!("Width: {}, Height {}", self.width, self.height),
            glyphs,
            &context.draw_state,
            context.transform.trans(20.0, 50.0),
            graphics,
        ) {
            println!("Text rendering error {:?}", error);
        }
    }
}

impl Default for MenuView {
    fn default() -> Self {
        Self {
            width: 5,
            height: 5,
        }
    }
}

impl MenuView {
    pub fn new(width: usize, height: usize) -> Self {
        Self { width, height }
    }
}
