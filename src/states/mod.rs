pub use piston_window::*;

pub mod game;
pub mod menu;

use self::game::GameView;
use self::menu::MenuView;

pub trait State {
    fn handle_event(&mut self, event: &Event) -> Option<CurrentState>;
    fn draw(&self, context: &Context, graphics: &mut G2d, glyphs: &mut Glyphs);
}

#[derive(Debug)]
pub enum CurrentState {
    Menu(MenuView),
    Game(GameView),
    Exited,
}

impl State for CurrentState {
    fn handle_event(&mut self, event: &Event) -> Option<CurrentState> {
        use self::CurrentState::*;

        let transition = match self {
            Menu(state) => state.handle_event(event),
            Game(state) => state.handle_event(event),
            Exited => None,
        };

        if let Some(next_state) = transition {
            *self = next_state;
        }

        None
    }

    fn draw(&self, context: &Context, graphics: &mut G2d, glyphs: &mut Glyphs) {
        use self::CurrentState::*;
        match self {
            Menu(state) => state.draw(context, graphics, glyphs),
            Game(state) => state.draw(context, graphics, glyphs),
            Exited => {}
        }
    }
}

impl Default for CurrentState {
    fn default() -> Self {
        self::CurrentState::Menu(Default::default())
    }
}
