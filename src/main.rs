extern crate find_folder;
extern crate piston_window;
extern crate rand;

mod constants;
mod states;

use piston_window::*;

use states::{CurrentState, State};

fn main() {
    let mut window: PistonWindow = WindowSettings::new("Hello Piston!", (640, 480))
        // .exit_on_esc(true)
        .build()
        .unwrap_or_else(|e| panic!("Failed to build PistonWindow: {}", e));

    let font = &find_folder::Search::ParentsThenKids(3, 3)
        .for_folder("assets")
        .expect("Failed to locate assets folder")
        .join("FiraSans-Regular.ttf");

    let mut glyphs = Glyphs::new(font, window.factory.clone(), TextureSettings::new()).unwrap();

    let mut state: CurrentState = Default::default();
    let state_ref = &mut state;

    while let Some(event) = window.next() {
        match *state_ref {
            CurrentState::Exited => {
                window.should_close();
                return;
            }
            _ => {}
        }

        match state_ref.handle_event(&event) {
            Some(next_state) => {
                *state_ref = next_state;
            }
            None => {}
        }

        window.draw_2d(&event, |c, g| {
            (*state_ref).draw(&c, g, &mut glyphs);
        });
    }
}
